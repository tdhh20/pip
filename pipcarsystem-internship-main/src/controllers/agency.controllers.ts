import { BadRequestError, DataNotFoundError } from '@pippip/pip-system-common';
import { Request, Response } from 'express';
import _Agency from '../models/agency.model';
import _Car from '../models/car.model';
import _Driver from '../models/driver.model';
import _Register, { TRegister } from '../models/register.model';
import { convertToNumber, getRandomInt, getTotalPage } from '../utils';

/**
 * [ADMIN,PM] getAgencyList
 * TODO:
 * [x] - Phân trang, Tìm kiếm
 * [] - Bộ lọc (status, ....)
 * [] - Sắp xếp
 */
export const getAgencyList = async (req: Request, res: Response): Promise<void> => {
  const { status, keyword, page, limit, isTransportation, isDriver, hasCar, sort } = req.query; // => all string
  let filter: any = {};

  if (status) {
    filter.status = status;
  }

  if (keyword) {
    const regex = new RegExp(`${keyword}`, 'i');
    const regexCond = { $regex: regex };
    console.log(regexCond);
    filter['$or'] = [{ name: regexCond }, { phone: regexCond }];
  }

  if (isTransportation) {
    filter.isTransportation = isTransportation;
  }

  if (isDriver) {
    filter.isDriver = isDriver;
  }

  if (hasCar) {
    filter.hasCar = { $gte: convertToNumber(hasCar) };
  }

  const currentPage = convertToNumber(page) || 1;

  const limitNumber = convertToNumber(limit) || 10;

  const totalAgency = await _Agency.countDocuments(filter);

  const foundAgencyList = await _Agency
    .find(filter)
    .skip((currentPage - 1) * limitNumber)
    .limit(limitNumber)
    .sort([
      ['status', 1],
      ['updatedAt', -1],
    ]);

  res.status(200).json({
    status: 1,
    message: 'Get Agency List Successfully.',
    data: {
      agency_list: foundAgencyList,
      meta_data: {
        page: currentPage,
        limit: limitNumber,
        total: totalAgency,
        totalPage: getTotalPage(totalAgency, limitNumber),
      },
    },
  });
};

/**
 * [ADMIN,PM] getAgencyDetail
 */
export const getAgencyDetail = async (req: Request, res: Response): Promise<void> => {
  const { agencyId } = req.params;
  const foundAgency = await _Agency.findById(agencyId);
  res.status(200).json({
    status: 1,
    message: 'Get Agency Profile Successfully.',
    data: {
      agency_detail: foundAgency,
    },
  });
};

/**
 * [ADMIN,PM] updateAgencyDetail
 */
export const updateAgencyDetail = async (req: Request, res: Response): Promise<void> => {
  const { agencyId } = req.params;
  // const {name,phone,address,isDriver,isTransportation,lat,lat_address,long,long_address,point,rank,updated_gps_time,status} = req.body;
  const {
    hasCar,
    refresh_token,
    code,
    phone,
    name,
    address,
    isDriver,
    isTransportation,
    lat,
    lat_address,
    long,
    long_address,
    point,
    rank,
    status,
    updated_gps_time,
    imageUrl,
  } = req.body;

  const foundAgency = await _Agency.findById(agencyId);
  if (!foundAgency) throw new DataNotFoundError();

  let newData = {
    code,
    phone,
    name,
    address,
    isDriver,
    isTransportation,
    lat,
    lat_address,
    long,
    long_address,
    point,
    rank,
    status,
    updated_gps_time,
    imageUrl,
  };
  if (!!code && foundAgency.code !== code) {
    newData.code = code;
    const updatedAgency = await _Agency.findByIdAndUpdate(
      agencyId,
      { ...newData, refresh_token: 'EMPTY' },
      {
        new: true,
        omitUndefined: true,
      },
    );
  } else {
    const updatedAgency = await _Agency.findByIdAndUpdate(agencyId, newData, {
      new: true,
      omitUndefined: true,
    });
  }

  res.status(200).json({
    status: 1,
    message: 'Update Agency Profile Successfully.',
  });
};

/**
 * [ADMIN,PM] createAgency
 */
/**
 * [ADMIN,PM] createAgency
 */
/**
 * [ADMIN,PM] createAgency
 */
export const createAgency = async (req: Request, res: Response): Promise<void> => {
  const {
    name,
    phone,
    address,
    isDriver,
    isTransportation,
    lat,
    lat_address,
    long,
    long_address,
    point,
    rank,
    updated_gps_time,
    imageUrl,
  } = req.body;

  const generatedCode = getRandomInt().toString();

  // Check if the account is already registered
  const foundAgency = await _Agency.findOne({ phone });
  if (foundAgency) throw new BadRequestError('This account is already registered');

  // Create register data
  const registerData: TRegister = {
    code: generatedCode,
    phone,
    name,
    status: 1,
    isAgency: true,
    isDriver,
    isTransportation,
  };

  // Save or update register data
  let foundRegister = await _Register.findOne({ phone });
  if (!foundRegister) {
    await new _Register(registerData).save();
  } else {
    foundRegister = await _Register.findOneAndUpdate({ phone }, registerData, {
      new: true,
      omitUndefined: true,
    });
  }

  // Create new agency
  const newAgency = await new _Agency({
    code: generatedCode,
    name,
    phone,
    address,
    isDriver,
    isTransportation,
    lat,
    lat_address,
    long,
    long_address,
    point,
    rank,
    updated_gps_time,
    imageUrl,
  }).save();

  if (isDriver && isTransportation) {
    const carCount = await _Car.countDocuments({ agency_id: newAgency._id });
    if (carCount === 1) {
      const car = await _Car.findOne({ agency_id: newAgency._id });
      if (car) {
        const driver = await _Driver.findOneAndUpdate(
          { car_id: null },
          { car_id: car._id },
          { new: true, omitUndefined: true },
        );
        if (driver) {
          await _Car.findByIdAndUpdate(car._id, { driver_id: driver._id });
        }
      }
    }
  }
  if (isTransportation) {
    const carCount = await _Car.countDocuments({ agency_id: newAgency._id });
    await _Agency.findByIdAndUpdate(newAgency._id, { hasCar: carCount }, { new: true });
  }

  res.status(200).json({
    status: 1,
    message: 'New agency created',
  });
};

/**
 * [ADMIN,PM] blockAgency
 */
export const blockAgency = async (req: Request, res: Response): Promise<void> => {
  const { agencyId } = req.params;
  const updatedAgency = await _Agency.findByIdAndUpdate(
    agencyId,
    { status: 2, refresh_token: 'EMPTY' },
    { new: true },
  );
  res.status(200).json({
    status: 1,
    message: 'Block Agency Successfully.',
  });
};

/**
 * [ADMIN,PM] unblockAgency
 */
export const unblockAgency = async (req: Request, res: Response): Promise<void> => {
  const { agencyId } = req.params;
  const updatedAgency = await _Agency.findByIdAndUpdate(agencyId, { status: 1 }, { new: true });
  res.status(200).json({
    status: 1,
    message: 'Unblock Agency Successfully.',
  });
};
