import styled from '@emotion/styled';
import { Typography } from 'antd';
import WithAuth from 'src/hooks/withAuth';
import { useAppSelector } from 'src/redux/store';

function Page() {
  const { data: userState } = useAppSelector((s) => s.user);

  return (
    <PageWrapper className='main-page'>
      <Typography.Paragraph>normalUser:{JSON.stringify(userState)}</Typography.Paragraph>
    </PageWrapper>
  );
}

const PageWrapper = styled.main``;

export default WithAuth(Page);
