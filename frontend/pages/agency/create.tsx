/* eslint-disable @next/next/no-img-element */
import styled from '@emotion/styled';
import {
  Card,
  Checkbox,
  Col,
  Divider,
  Form,
  Input,
  InputNumber,
  Row,
  Slider,
  Space,
  Statistic,
  Typography,
} from 'antd';
import type { DefaultOptionType } from 'antd/es/cascader';
import { useRouter } from 'next/router';
import { BsBoxArrowUpRight, BsPlusLg } from 'react-icons/bs';
import { IoMdCar } from 'react-icons/io';
import { MdEmojiTransportation } from 'react-icons/md';
import { useMediaQuery } from 'react-responsive';
import PMBreadcrumb from 'src/components/breadcrumb/PMBreadcrumb';
import Button from 'src/components/button/Button';
import PointSlideFormItem from 'src/components/form/PointSlideFormItem';
import InputCode from 'src/components/input/InputCode';
import useApp from 'src/hooks/useApp';
import WithAuth from 'src/hooks/withAuth';
import { useCreateAgencyMutation } from 'src/redux/query/agency.query';
import { ErrorCode } from 'src/types/response.types';
import { getRandomInt, vietnameseSlug } from 'src/utils/utils';
import { mappedErrorToFormError } from 'src/utils/utils-error';

const filter = (inputValue: string, path: DefaultOptionType[]) =>
  path.some(
    (option) =>
      vietnameseSlug(option.label as string, ' ').indexOf(vietnameseSlug(inputValue, ' ')) > -1,
  );

import { useState } from 'react';

function CreateAgencyPage() {
  const { push } = useRouter();
  const { message } = useApp();
  const [form] = Form.useForm();
  const mediaAbove767 = useMediaQuery({ minWidth: 767 });
  const [createAgency, { isLoading }] = useCreateAgencyMutation();

  const [imageUrl, setImageUrl] = useState('');

  const handleImageUrlChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = e.target.value;
    if (newValue.startsWith('http://') || newValue.startsWith('https://')) {
      setImageUrl(newValue);
    } else {
      console.error('Invalid image URL');
    }
  };

  const increase = () => {
    const current = form.getFieldValue('point');
    let newCount = +current + 100;
    if (newCount > 1000) {
      newCount = 1000;
    }
    form.setFieldValue('point', newCount);
  };

  const decline = () => {
    const current = form.getFieldValue('point');
    let newCount = +current - 100;
    if (newCount < 0) {
      newCount = 0;
    }
    form.setFieldValue('point', newCount);
  };

  const handleCreateAgency = ({ confirm, lat_address, long_address, ...formData }: any) => {
    const newFormData = {
      lat: lat_address,
      long: long_address,
      lat_address,
      long_address,
      ...formData,
    };
    createAgency({ data: newFormData })
      .unwrap()
      .then((res) => {
        message.success({
          content: (
            <Space direction='vertical' size={2} align='start'>
              <div>{res.message}</div>
              <Space
                style={{ color: '#52c41a', textDecoration: 'underline' }}
                wrap={false}
                size={2}
              >
                <BsBoxArrowUpRight /> <span>Check now</span>
              </Space>
            </Space>
          ),
          style: { cursor: 'pointer' },
          onClick: () => {
            push('/agency');
            message.destroy();
          },
          duration: 3,
        });
        form.resetFields();
      })
      .catch((err) => {
        if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
          message.error(err.error[0].message);
        if (err.response_code === ErrorCode.RequestValidationError) {
          form.setFields(mappedErrorToFormError(err.error));
        }
      });
  };

  return (
    <PageWrapper className='main-page'>
      <Row className='page-header' gutter={[0, 12]}>
        {!mediaAbove767 && (
          <Col span={24}>
            <PMBreadcrumb />
          </Col>
        )}
        <Col flex='auto'>
          <Space>
            <Typography.Title className='page-title' level={2}>
              Add new Agency
            </Typography.Title>
          </Space>
        </Col>
        <Col flex='none'></Col>
      </Row>
      <Form
        form={form}
        layout='vertical'
        size='large'
        onFinish={handleCreateAgency}
        disabled={isLoading}
        autoComplete='off'
        initialValues={{
          hasCar: 0,
          point: 1000,
          rank: 0,
          isDriver: false,
          isTransportation: false,
          lat_address: '21.004366',
          long_address: '105.846573',
          lat: '21.004366',
          long: '105.846573',
          updated_gps_time: 0,
        }}
      >
        <Row gutter={[24, 24]}>
          <Col flex='auto' className='col-left'>
            <Card>
              <Form.Item
                name='name'
                label='Name'
                rules={[{ required: true, message: '• Name is required' }]}
              >
                <Input type='text' placeholder='Name...' />
              </Form.Item>
              <Form.Item
                name='phone'
                label='PhoneNumber'
                rules={[{ required: true, message: '• Phone is required' }]}
              >
                <Input type='tel' placeholder='PhoneNumber...' />
              </Form.Item>
            </Card>
            <Card>
              <Form.Item name='imageUrl' label='Avatar'>
                <Input
                  type='String'
                  onChange={handleImageUrlChange}
                  placeholder='Enter an image URL...'
                />
              </Form.Item>
              <Form.Item name='preview_picture' label='Preview Avatar'>
                {imageUrl ? (
                  <div className='avatar-preview-container'>
                    <img src={imageUrl} alt='Agency Image' className='avatar-preview' />
                  </div>
                ) : (
                  <div className='avatar-preview-container'>
                    <img
                      src='/default-avatar.jpg'
                      alt='Default Avatar'
                      className='avatar-preview'
                    />
                  </div>
                )}
              </Form.Item>
            </Card>
            <Card>
              <Form.Item
                name='address'
                label='Address'
                rules={[{ required: true, message: '• Address is required' }]}
              >
                <Input.TextArea placeholder='Address...' autoSize={{ minRows: 2 }} showCount />
              </Form.Item>
              <Form.Item label='Map' tooltip='Lat | Long'>
                <Space.Compact block size='large'>
                  <Form.Item
                    name='lat_address'
                    noStyle
                    rules={[{ required: true, min: 0, message: '• lat_address is required' }]}
                  >
                    <InputNumber<string>
                      placeholder='Lat...'
                      stringMode
                      step='0.00001'
                      style={{ width: '100%' }}
                    />
                  </Form.Item>
                  <Form.Item
                    name='long_address'
                    noStyle
                    rules={[{ required: true, message: '• long_address is required' }]}
                  >
                    <InputNumber<string>
                      placeholder='Long...'
                      stringMode
                      step='0.00001'
                      style={{ width: '100%' }}
                    />
                  </Form.Item>
                </Space.Compact>
              </Form.Item>
              {/* {!!latAddressForm && !!longAddressForm && (
                <iframe loading='lazy' src={iframeMapSrc}></iframe>
              )} */}
              <iframe
                src='https://www.google.com/maps/embed?language=en&pb=!1m14!1m8!1m3!1d931.1760760275844!2d105.8469656!3d21.004487!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76827aaaab%3A0xf0580eb2ff0e1b64!2zVHLGsOG7nW5nIEPDtG5nIE5naOG7hyBUaMO0bmcgVGluIFRydXnhu4FuIFRow7RuZyAtIMSQ4bqhaSBI4buNYyBCw6FjaCBraG9hIEjDoCBu4buZaQ!5e0!3m2!1svi!2s!4v1673413674924!5m2!1svi!2s'
                loading='lazy'
                referrerPolicy='no-referrer-when-downgrade'
              ></iframe>
            </Card>

            <Card>
              <Form.Item
                name='code'
                label='Code'
                rules={[{ required: true, type: 'string', message: '• Code is required' }]}
              >
                <InputCode
                  onClickGenerate={() => {
                    form.setFieldValue('code', String(getRandomInt()));
                  }}
                />
              </Form.Item>
            </Card>
          </Col>
          <Col flex='320px' className='col-right'>
            <Card>
              <Form.Item label='Type' required>
                <div className='checkbox-type-group'>
                  <Form.Item
                    name='isDriver'
                    valuePropName='checked'
                    noStyle
                    rules={[{ type: 'boolean', message: '• isDriver is invalid' }]}
                  >
                    <Checkbox className='checkbox-item'>
                      <IoMdCar size={20} />
                      <span>Driver</span>
                    </Checkbox>
                  </Form.Item>
                  <Form.Item
                    name='isTransportation'
                    valuePropName='checked'
                    noStyle
                    rules={[{ type: 'boolean', message: '• isTransportation is invalid' }]}
                  >
                    <Checkbox className='checkbox-item'>
                      <MdEmojiTransportation size={24} />
                      <span>Transportation</span>
                    </Checkbox>
                  </Form.Item>
                </div>
              </Form.Item>
              <div>
                <Statistic title='Cars owned' value={0} prefix={<IoMdCar size={32} />} />
              </div>
              <Divider />
              <PointSlideFormItem />
              <Divider />
              <Form.Item
                name='rank'
                label='Rank'
                help={'Rate agency to return agency priority search'}
                tooltip='Coming soon...'
              >
                <Slider
                  disabled
                  step={1}
                  max={5}
                  min={0}
                  marks={{
                    0: { style: { transform: 'translateX(-6px)' }, label: 'Normal' },
                    3: 'High',
                    5: {
                      style: { transform: 'translateX(-32px)' },
                      label: <strong>Highest</strong>,
                    },
                  }}
                />
              </Form.Item>
            </Card>
          </Col>
        </Row>

        <Divider />
        <Form.Item className='actions-container'>
          <Button
            htmlType='reset'
            block
            disabled={isLoading}
            onClick={(e) => {
              e.stopPropagation();
            }}
          >
            Reset
          </Button>
          <Button htmlType='submit' block loading={isLoading} type='primary' icon={<BsPlusLg />}>
            Create
          </Button>
        </Form.Item>
      </Form>
    </PageWrapper>
  );
}

const PageWrapper = styled.main`
  padding: 0 24px;
  .ant-card-body {
    position: relative;
  }
  .ant-form-vertical {
    .col-right,
    .col-left {
      display: flex;
      flex-direction: column;
      gap: 24px;
    }
    .avatar-preview-container {
      display: flex;
      justify-content: center;
      align-items: center;
      margin-top: 20px;
    }
    .avatar-preview {
      width: 150px;
      height: 150px;
      border-radius: 50%;
      object-fit: cover;
    }
    .point-container {
      position: relative;
      .point-actions {
        position: absolute;
        top: 0;
        right: 0;
      }
    }
    .ant-input-textarea-show-count::after {
      position: absolute;
      top: 0;
      right: 0;
      transform: translateY(calc(-100% - 8px));
    }
    .actions-container {
      .ant-form-item-control-input-content {
        display: flex;
        justify-content: flex-end;
        align-items: center;
        gap: 24px;
      }
      & button[type='reset'] {
        width: 160px;
      }
      & button[type='submit'] {
        width: 296px;
      }
    }
  }
  .horizontal-form-item {
    .ant-form-item-row {
      flex-direction: row;
    }
  }
  .checkbox-type-group {
    display: flex;
    flex-wrap: wrap;
    gap: 12px;
    .checkbox-item {
      padding: 8px 24px 8px 8px;
      border: 1px solid #d9d9d9;
      position: relative;
      align-items: center;
      border-radius: 8px;
      margin-left: 0;
      & span:last-of-type {
        display: flex;
        align-items: center;
        gap: 4px;
      }
      &.ant-checkbox-wrapper-checked {
        border: 1px solid ${({ theme }) => theme.colorPrimary};
        color: ${({ theme }) => theme.colorPrimary};
      }
      .ant-checkbox {
        position: absolute;
        top: 0;
        right: 0;
        transform: translate(-50%, 50%);
      }
    }
  }

  @media screen and (max-width: 767.98px) {
    .ant-form-vertical {
      .col-right {
        flex: 0 0 auto !important;
      }
    }
  }
  @media screen and (max-width: 400.98px) {
    padding: 0 12px;
    .page-header {
      .page-title {
        font-size: 22px;
      }
    }
  }
`;

export default WithAuth(CreateAgencyPage);
