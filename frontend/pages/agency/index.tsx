import styled from '@emotion/styled';
import {
  Card,
  Col,
  Divider,
  Dropdown,
  List,
  Row,
  Segmented,
  Space,
  Tag,
  theme,
  Typography,
} from 'antd';
import { useState } from 'react';
import { BsPlusLg, BsSortDownAlt } from 'react-icons/bs';
import { FaLock, FaUnlockAlt } from 'react-icons/fa';
import { IoMdCar } from 'react-icons/io';
import { useMediaQuery } from 'react-responsive';
import TypeAgencyAvatar from 'src/components/avatar/TypeAgencyAvatar';
import PMBreadcrumb from 'src/components/breadcrumb/PMBreadcrumb';
import Button from 'src/components/button/Button';
import LocalSearch from 'src/components/input/LocalSearch';
import StyledListContainer from 'src/components/list/StyledListContainer';
import Link from 'src/components/next/Link';
import useChangeStatusAgency from 'src/hooks/useChangeStatusAgency';
import useDebounce from 'src/hooks/useDebounce';
import WithAuth from 'src/hooks/withAuth';
import { TListFilter, useGetAgencyListQuery } from 'src/redux/query/agency.query';
import { TAgency } from 'src/types/agency.types';
import { TMetaBase } from 'src/types/response.types';

const initialFilterValue: TListFilter = { page: 1, limit: 10 };

function AgencyListPage() {
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const mediaAbove767 = useMediaQuery({ minWidth: 767 });
  const mediaAbove500 = useMediaQuery({ minWidth: 500 });
  const [filterValue, setFilterValue] = useState<TListFilter>(initialFilterValue);
  const [selectedTab, setSelectedTab] = useState<string>('');
  const debouncedFilter = useDebounce(filterValue, 500);
  const {
    data: agenciesFilteredQuery,
    isSuccess: getAgenciesSuccess,
    isFetching: getAgenciesFetching,
  } = useGetAgencyListQuery(debouncedFilter, { refetchOnMountOrArgChange: true });

  const agenciesFilteredData = getAgenciesSuccess
    ? agenciesFilteredQuery?.data?.agency_list || []
    : [];
  const filteredMetaData: TMetaBase | undefined = getAgenciesSuccess
    ? agenciesFilteredQuery?.data?.meta_data
    : undefined;

  const { handleChangeStatus, blockLoading, unBlockLoading } = useChangeStatusAgency();

  const handleLocalSearch = ({ keySearch }: { keySearch: string }) => {
    setFilterValue({ ...filterValue, keyword: keySearch });
  };
  const handleSelectSort = (value: string) => {
    setFilterValue({ ...filterValue, sort: value });
  };
  const handleTabChange = (value: string) => {
    const [key, v] = value.split('_');
    setFilterValue({ ...initialFilterValue, ...{ [key]: v } });
    setSelectedTab(value);
  };

  return (
    <PageWrapper className='main-page'>
      <Row className='page-header'>
        <Col flex='auto'>
          {!mediaAbove767 && <PMBreadcrumb />}
          <Typography.Title className='page-title' level={2}>
            Agency List
          </Typography.Title>
        </Col>
        <Col flex='none'>
          <Link href='/agency/create'>
            <Button type='primary' icon={<BsPlusLg />}>
              {mediaAbove500 ? 'Add new Agency' : 'Add'}
            </Button>
          </Link>
        </Col>
      </Row>
      <Card
        style={{ width: '100%' }}
        bodyStyle={{ padding: 0 }}
        tabList={[
          {
            key: '',
            tab: 'ALL',
          },
          {
            key: 'status_1',
            tab: 'Active',
          },
          {
            key: 'status_2',
            tab: 'Blocked',
          },
          {
            key: 'isTransportation_true',
            tab: 'Transportation',
          },
          {
            key: 'isDriver_true',
            tab: 'Driver',
          },
        ]}
        activeTabKey={selectedTab}
        onTabChange={(key) => handleTabChange(key)}
      >
        <Space
          className='header-filter-container'
          wrap={false}
          split={<Divider type='vertical' />}
          size={0}
        >
          <LocalSearch
            placeholder={mediaAbove500 ? 'Search by Name, Phone...' : 'by Name, Phone...'}
            onFinish={handleLocalSearch}
            onValuesChange={(changedValue, values) => handleLocalSearch(values)}
          />
          <Dropdown
            menu={{
              items: [
                { key: 'name_asc', label: `Tên A->Z` },
                { key: 'name_desc', label: `Tên Z->A` },
                { key: 'createdAt_desc', label: 'Ngày tạo mới nhất' },
                { key: 'createdAt_asc', label: 'Ngày tạo cũ nhất' },
              ],
              selectable: true,
              selectedKeys: !!filterValue.sort ? [filterValue.sort] : undefined,
              onSelect: ({ key }) => handleSelectSort(key),
            }}
            arrow={{ pointAtCenter: true }}
            placement='bottomRight'
          >
            <Button size='large' block icon={<BsSortDownAlt size={20} />}></Button>
          </Dropdown>
        </Space>
        <ListContainer
          loading={getAgenciesFetching}
          dataSource={agenciesFilteredData}
          pagination={{
            metadata: filteredMetaData,
            onChange: (page, pageSize) => setFilterValue({ ...filterValue, page, limit: pageSize }),
          }}
          renderItem={(item) => (
            <List.Item className='item-container'>
              <Space split={<Divider type='vertical' />}>
                <List.Item.Meta
                  style={{ width: 220, maxWidth: 220 }}
                  avatar={
                    <Link href={`/agency/${item._id}`}>
                      {item.imageUrl ? (
                        <img
                          src={item.imageUrl}
                          alt={`${item.name} Avatar`}
                          style={{ width: 48, height: 48, borderRadius: '50%' }}
                        />
                      ) : (
                        <TypeAgencyAvatar
                          size={48}
                          offset={[0, 34]}
                          isActive={item.status === 1}
                          isDriver={item.isDriver}
                          isTransportation={item.isTransportation}
                        />
                      )}
                    </Link>
                  }
                  title={
                    <Link href={`/agency/${item._id}`} className='user-name'>
                      {item.name}
                    </Link>
                  }
                  description={item.phone}
                />

                {mediaAbove500 && (
                  <Tag color='geekblue' icon={<IoMdCar />}>
                    {item.hasCar}
                  </Tag>
                )}
              </Space>
              <Space split={<Divider type='vertical' />}>
                {mediaAbove767 && (
                  <Space>
                    {item.isTransportation && <Tag color='geekblue'>Transportation</Tag>}
                    {item.isDriver && <Tag color='blue'>Driver</Tag>}
                  </Space>
                )}

                <Segmented
                  value={item.status === 1 ? 'unlock' : 'lock'}
                  className={
                    item.status === 1 ? 'ant-segmented-status unlock' : 'ant-segmented-status lock'
                  }
                  options={[
                    {
                      value: 'unlock',
                      icon: <FaUnlockAlt />,
                    },
                    {
                      value: 'lock',
                      icon: <FaLock />,
                    },
                  ]}
                  disabled={blockLoading || unBlockLoading}
                  onChange={(v) => handleChangeStatus(item._id, v as string)}
                />
              </Space>
            </List.Item>
          )}
        ></ListContainer>
      </Card>
    </PageWrapper>
  );
}

const PageWrapper = styled.main`
  padding: 0 24px 24px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  .page-header {
    padding: 24px 0;
    .page-title {
      margin: 0;
    }
  }

  .header-filter-container {
    width: 100%;
    padding: 24px 24px 24px;
    .ant-space-item:first-of-type {
      flex: 1 1 auto;
    }
  }

  @media screen and (max-width: 767.98px) {
    & {
      height: 100%;
      padding: 0 12px;
    }
  }

  @media screen and (max-width: 400.98px) {
    & {
      padding: 0 0 24px;
      height: 100%;
    }
    .page-header {
      padding: 24px;
    }
    .ant-card {
      border: none !important;
      border-radius: 0;
    }
  }
`;

const ListContainer = styled(StyledListContainer<TAgency>)``;

export default WithAuth(AgencyListPage);
